package com.citi.training.staff.dao;

import java.util.List;

import com.citi.training.staff.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

/**
 * This class is an alternative to using the EmployeeMongoRepo class.
 * 
 * This implementation uses the org.springframework.data.mongodb.core.MongoTemplate to
 * interface to the MongoDB.
 * 
 * This allows for more complex querying that the MongoRepository implementation.
 * However, it does require more code to be written.
 * 
 * The @Profile annotation means that this class will only be included by spring when the
 * MongoTemplate profile is active. This can be done by adding the following line to
 * application.properties:
 * spring.profiles.active=mongoTemplate
*/
@Profile("MongoTemplate")
@Component
public class EmployeeMongoTemplate implements EmployeeRepo {

    @Autowired
    private MongoTemplate mongoTemplate;

    public Employee save(Employee employee) {
        return mongoTemplate.save(employee);
    }

    public List<Employee> findAll() {
        return mongoTemplate.findAll(Employee.class);
    }

    public void deleteById(String id) {
        mongoTemplate.remove(new Query(Criteria.where("id").is(id)), Employee.class);
    }
}

